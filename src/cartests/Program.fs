﻿open Expecto
open Car.CCode
open Car.CCodeTypes
open System.Net

[<Tests>]
let tests = testList "A test group" [

                    testCase "Test search parameter return" <| fun _ ->
                        try
                            let result = getSearchParams().Makes.GetValue(0).ToString()
                            let expected = ""
                            Expect.notEqual expected result "Should not equal"
                            printfn "Test search parameter return: Passed" 
                        with e ->
                            printfn "Test search parameter return: Failed"

                    testCase "Test search not empty" <| fun _ ->
                        try
                            let data = "{\"Make\" : \"\",\"Model\" : \"\",\"YearUpperBound\" : 9999,\"YearLowerBound\" : 9999,\"Colour\" : \"\",\"BodyType\" : \"\",\"Doors\" : 10,\"GearBoxType\" : \"\",\"MilesUpperBound\" : 1000000,\"MilesLowerBound\" : 1000000,\"EngineSizeUpperBound\" : 99.9,\"EngineSizeLowerBound\" : 99.9,\"SixtyTimeUpperBound\" : 99.9,\"SixtyTimeLowerBound\" :  99.9,\"FuelType\" : \"\",\"DriveTrainType\" : \"\",\"Co2UpperBound\" : 9999,\"Co2LowerBound\" : 9999, \"PriceUpperBound\" : 999999999.9,\"PriceLowerBound\" : 999999999.9}"
                            let result = (Search data).[0].CarID.ToString()
                            let expected = ""
                            Expect.notEqual expected result "Should not equal"
                            printfn "Test search not empty: Passed" 
                        with e ->
                            printfn "Test search not empty: Failed"


                    testCase "Test searchAll not empty" <| fun _ ->
                        try
                            let data = "{\"Make\" : \"\",\"Model\" : \"\",\"YearUpperBound\" : 9999,\"YearLowerBound\" : 9999,\"Colour\" : \"\",\"BodyType\" : \"\",\"Doors\" : 10,\"GearBoxType\" : \"\",\"MilesUpperBound\" : 1000000,\"MilesLowerBound\" : 1000000,\"EngineSizeUpperBound\" : 99.9,\"EngineSizeLowerBound\" : 99.9,\"SixtyTimeUpperBound\" : 99.9,\"SixtyTimeLowerBound\" :  99.9,\"FuelType\" : \"\",\"DriveTrainType\" : \"\",\"Co2UpperBound\" : 9999,\"Co2LowerBound\" : 9999, \"PriceUpperBound\" : 999999999.9,\"PriceLowerBound\" : 999999999.9}"
                            let result = (SearchAll data).[0].CarID.ToString()
                            let expected = ""
                            Expect.notEqual expected result "Should not equal"
                            printfn "Test searchAll not empty: Passed" 
                        with e ->
                            printfn "Test searchAll not empty: Failed"

                    testCase "Test byDealership not empty" <| fun _ ->
                        try
                            let data = "{\"Dealership\" : \"Middlesbrough\"}"
                            let result = (GetByDealershipID data).[0].CarID.ToString()
                            let expected = ""
                            Expect.notEqual expected result "Should not equal"
                            printfn "Test byDealership not empty: Passed" 
                        with e ->
                            printfn "Test byDealership not empty: Failed"
                    
                    testCase "Test insert fail response" <| fun _ ->
                        try
                            let result = (insertNewCar "")
                            let expected = HttpStatusCode.InternalServerError
                            Expect.equal expected result "Should equal"
                            printfn "Test insert fail response: Passed" 
                        with e ->
                            printfn "Test insert fail response: Failed"

                    testCase "Test update fail response" <| fun _ ->
                        try
                            let result = (updateCar "")
                            let expected = HttpStatusCode.InternalServerError
                            Expect.equal expected result "Should equal"
                            printfn "Test update fail response: Passed" 
                        with e ->
                            printfn "Test update fail response: Failed"
                    ]

[<EntryPoint>]
let main args = 
    args |> ignore
    printfn "Begin Tests: "
    let _r = runTestsInAssembly defaultConfig args
    0 // return an integer exit code
