namespace Car

module CCodeDictionary =
    
    let carBodyType = dict["Hatchback", 1; "Coupe", 2; "Convertible", 3; "Estate", 4; "SUV", 5; "Saloon", 6; "Pickup", 7]

    let carGearBoxType = dict["Manual", 1; "Automatic", 2]

    let carFuelType = dict["Petrol", 1; "Diesal", 2]

    let carDriveTrainType = dict["FWD", 1; "RWD", 2; "AWD", 3]
    