namespace Car

open System
open System.Net
open FSharp.Core
open DataLayer.DLCode
open Car.CCodeDictionary
open Car.CCodeTypes
open Newtonsoft.Json

module CCode =
    
    let nullable value = new System.Nullable<_>(value)
    
    
    let private emptyCarSearch = {
                                    CarID = nullable 0
                                    Make = ""
                                    Model = ""
                                    Year = nullable 0s
                                    BodyType = ""
                                    GearBoxType = ""
                                    Miles = nullable 0
                                    EngineSize = nullable 0.0
                                    HorsePower = nullable 0s
                                    FuelType = ""
                                    Price = nullable 0.0
                                    ImageString = ""
                                    }

    let private emptyCarSearchAll = {
                                    CarID = nullable 0
                                    Make = ""
                                    Model = ""
                                    Year = nullable 0s
                                    BodyType = ""
                                    GearBoxType = ""
                                    Miles = nullable 0
                                    EngineSize = nullable 0.0
                                    HorsePower = nullable 0s
                                    FuelType = ""
                                    Price = nullable 0.0
                                    ImageString = ""
                                    Active = nullable false
                                    }

    let private emptyCar = {
                            CarID = nullable 0
                            Make = ""
                            Model = ""
                            Year = nullable 0s
                            Colour = ""
                            BodyType = ""
                            Doors = nullable 0uy
                            GearBoxType = ""
                            Miles = nullable 0
                            EngineSize = nullable 0.0
                            HorsePower = nullable 0s
                            SixtyTime = nullable 0.0
                            FuelType = ""
                            DriveTrainType = ""
                            Co2 = nullable 0s
                            Price = nullable 0.0
                            ImageString = ""
                            Name = ""
                            Number = ""
                            Address = ""
                            City = ""
                            PostCode = ""
                            }

    let private emptyFullCar = {
                            CarID = nullable 0
                            Car = ""
                            Price = nullable 0.0
                            Sold = nullable false
                            Active = nullable false
                            DealershipID = nullable 0uy
                            }

    let emptySearchParam = {
                            Make = ""
                            Model = ""
                            YearUpperBound = nullable 9999
                            YearLowerBound = nullable 9999
                            Colour = ""
                            BodyType = ""
                            Doors = nullable 10
                            GearBoxType = ""
                            MilesUpperBound = nullable 1000000
                            MilesLowerBound = nullable 1000000
                            EngineSizeUpperBound = nullable 99.9
                            EngineSizeLowerBound = nullable 99.9
                            SixtyTimeUpperBound = nullable 99.9
                            SixtyTimeLowerBound = nullable 99.9
                            FuelType = ""
                            DriveTrainType = ""
                            Co2UpperBound = nullable 9999
                            Co2LowerBound = nullable 9999
                            PriceUpperBound = nullable 999999999.9
                            PriceLowerBound = nullable 999999999.9
                            }

    let AuthCheck (creds : staffAuth) = 
        let schema = dbSchema.GetDataContext()
        (query {
                        for row in schema.StaffAuthCheck(creds.UserName, (hash creds.Password).ToString()) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Position = row.Position
                                                      }) |> Seq.toArray).[0]
    
    let getSearchParams() =
        let schema = dbSchema.GetDataContext()
        let makes = query {
                        for row in schema.CarSearchMakes() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Make = row.Make
                                                      }.Make) |> Seq.toArray
        let year = query {
                        for row in schema.CarSearchGetYears() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Year = row.Num
                                                      }.Year) |> Seq.toArray

        let colour = query {
                        for row in schema.CarSearchModelColour() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Colour = row.Colour
                                                      }.Colour) |> Seq.toArray
        let miles = query {
                        for row in schema.CarSearchMiles() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Miles = row.Num
                                                      }.Miles) |> Seq.toArray
        
        let engineSize = query {
                        for row in schema.CarSearchEngineSize() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        EngineSize = System.Math.Round(row.Num.Value,1)
                                                      }.EngineSize) |> Seq.toArray
        let horsePower = query {
                        for row in schema.CarSearchHorsePower() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        HorsePower = row.Num
                                                      }.HorsePower) |> Seq.toArray

        let co2 = query {
                        for row in schema.CarSearchCo2() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Co2 = row.Num
                                                      }.Co2) |> Seq.toArray
        let price = query {
                        for row in schema.CarSearchPrice() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Price = row.Num
                                                      }.Price) |> Seq.toArray

        {
            Makes = makes
            Years = year
            Colour = colour
            Miles = miles
            EngineSize = engineSize
            HorsePower = horsePower
            Co2 = co2
            Price = price
            }

    let getSearchAllParams() =
        let schema = dbSchema.GetDataContext()
        let makes = query {
                        for row in schema.CarSearchAllMakes() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Make = row.Make
                                                      }.Make) |> Seq.toArray
        let year = query {
                        for row in schema.CarSearchAllGetYears() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Year = row.Num
                                                      }.Year) |> Seq.toArray

        let colour = query {
                        for row in schema.CarSearchAllModelColour() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Colour = row.Colour
                                                      }.Colour) |> Seq.toArray
        let miles = query {
                        for row in schema.CarSearchAllMiles() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Miles = row.Num
                                                      }.Miles) |> Seq.toArray
        
        let engineSize = query {
                        for row in schema.CarSearchAllEngineSize() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        EngineSize = System.Math.Round(row.Num.Value,1)
                                                      }.EngineSize) |> Seq.toArray
        let horsePower = query {
                        for row in schema.CarSearchAllHorsePower() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        HorsePower = row.Num
                                                      }.HorsePower) |> Seq.toArray

        let co2 = query {
                        for row in schema.CarSearchAllCo2() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Co2 = row.Num
                                                      }.Co2) |> Seq.toArray
        let price = query {
                        for row in schema.CarSearchAllPrice() do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Price = row.Num
                                                      }.Price) |> Seq.toArray

        {
            Makes = makes
            Years = year
            Colour = colour
            Miles = miles
            EngineSize = engineSize
            HorsePower = horsePower
            Co2 = co2
            Price = price
            }

    let getModelSearchParam(make : string) =
        let schema = dbSchema.GetDataContext()
        query {
                        for row in schema.CarSearchModel(make) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Model = row.Model
                                                        }.Model) |> Seq.toArray
        
    let getModelSearchAllParam(make : string) =
        let schema = dbSchema.GetDataContext()
        query {
                        for row in schema.CarSearchAllModel(make) do
                                select row
                            } |> Seq.map (fun row -> {
                                                        Model = row.Model
                                                        }.Model) |> Seq.toArray

    let Search (searchParam : String) =
        try
            let schema = dbSchema.GetDataContext()
            let search = try
                             JsonConvert.DeserializeObject<searchParam>(searchParam)
                         with ex ->
                             emptySearchParam
                             
            let query =
                query {
                    for row in schema.CarSearch(search.Make, search.Model, search.YearUpperBound, search.YearLowerBound, search.Colour, search.BodyType, search.Doors, search.GearBoxType, search.MilesUpperBound, search.MilesLowerBound, search.EngineSizeUpperBound, search.EngineSizeLowerBound, search.SixtyTimeUpperBound, search.SixtyTimeLowerBound, search.FuelType, search.DriveTrainType, search.Co2UpperBound, search.Co2LowerBound, search.PriceUpperBound, search.PriceLowerBound) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Make = row.Make
                                            Model = row.Model
                                            Year = row.Year
                                            BodyType = row.BodyType
                                            GearBoxType = row.GearBoxType
                                            Miles = row.Miles
                                            EngineSize = row.EngineSize
                                            HorsePower = row.HorsePower
                                            FuelType = row.FuelType
                                            Price = row.Price
                                            ImageString = row.CarString
                                            })
            query |> Seq.toArray
        with ex ->
            [|emptyCarSearch|]

    let SearchAll (searchParam : String) =
        try
            let schema = dbSchema.GetDataContext()
            let search = try
                             JsonConvert.DeserializeObject<searchParam>(searchParam)
                         with ex ->
                             emptySearchParam
                             
            let query =
                query {
                    for row in schema.CarSearchAll(search.Make, search.Model, search.YearUpperBound, search.YearLowerBound, search.Colour, search.BodyType, search.Doors, search.GearBoxType, search.MilesUpperBound, search.MilesLowerBound, search.EngineSizeUpperBound, search.EngineSizeLowerBound, search.SixtyTimeUpperBound, search.SixtyTimeLowerBound, search.FuelType, search.DriveTrainType, search.Co2UpperBound, search.Co2LowerBound, search.PriceUpperBound, search.PriceLowerBound) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Make = row.Make
                                            Model = row.Model
                                            Year = row.Year
                                            BodyType = row.BodyType
                                            GearBoxType = row.GearBoxType
                                            Miles = row.Miles
                                            EngineSize = row.EngineSize
                                            HorsePower = row.HorsePower
                                            FuelType = row.FuelType
                                            Price = row.Price
                                            ImageString = row.CarString
                                            Active = row.Active
                                            })
            query |> Seq.toArray
        with ex ->
            [|emptyCarSearchAll|]

    let GetByDealershipID (searchParam : String) =
        try
            let schema = dbSchema.GetDataContext()
            let search = try
                             JsonConvert.DeserializeObject<Dealership>(searchParam)
                         with ex ->
                             {Dealership = ""}
                             
            let query =
                query {
                    for row in schema.CarGetbyDealership(search.Dealership) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Car = row.Car
                                            Price = row.Price
                                            Image = row.CarString
                                            })
            query |> Seq.toArray
        with ex ->
            [|{
                CarID = nullable 0
                Car = ""
                Price = nullable 0.0
                Image = ""
                }|]

    let GetByReg (searchParam : String) =
        try
            let schema = dbSchema.GetDataContext()
            let search = try
                             JsonConvert.DeserializeObject<Reg>(searchParam)
                         with ex ->
                             {Reg = ""}
                             
            let query =
                query {
                    for row in schema.CarGetbyReg(search.Reg) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Car = row.Car
                                            Price = row.Price
                                            Image = row.CarString
                                            })
            query |> Seq.toArray
        with ex ->
            [|{
                CarID = nullable 0
                Car = ""
                Price = nullable 0.0
                Image = ""
                }|]
            
    let getCarDetails (carID : string) =
        try
            let schema = dbSchema.GetDataContext()
            let cID = JsonConvert.DeserializeObject<carID>(carID).carID
            let query =
                query {
                    for row in schema.CarGetDetails(nullable cID) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Make = row.Make
                                            Model = row.Model
                                            Year = row.Year
                                            Colour = row.Colour
                                            BodyType = row.BodyType
                                            Doors = row.Doors
                                            GearBoxType = row.GearBoxType
                                            Miles = row.Miles
                                            EngineSize = row.EngineSize
                                            HorsePower = row.HorsePower
                                            SixtyTime = row.SixtyTime
                                            FuelType = row.FuelType
                                            DriveTrainType = row.DriveTrainType
                                            Co2 = row.Co2
                                            Price = row.Price
                                            ImageString = row.CarString
                                            Name = row.Name
                                            Number = row.Number
                                            Address = row.Address
                                            City = row.Town_City
                                            PostCode = row.Postcode
                                            })
            query |> Seq.toArray
        with ex ->
            [|emptyCar|]

    let getFullCarDetails (carID : string) =
        try
            let schema = dbSchema.GetDataContext()
            let cID = JsonConvert.DeserializeObject<carID>(carID).carID
            let query =
                query {
                    for row in schema.CarGetFullDetails(nullable cID) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarId
                                            Car = row.Car
                                            Price = row.Price
                                            Sold = row.Sold
                                            Active = row.Active
                                            DealershipID = row.DealershipId
                                            })
            query |> Seq.toArray
        with ex ->
            [|emptyFullCar|]

    let ByCustomerID (data : string) =
        try
            let schema = dbSchema.GetDataContext()
            let id = try
                        JsonConvert.DeserializeObject<customerID>(data)
                     with ex -> {customerID = nullable 0}
            let query =
                query {
                    for row in schema.CarGetByCustomerID(id.customerID) do
                    select row
                } |> Seq.map (fun row -> {
                                            CarID = row.CarID
                                            Car = row.Car
                                            })
            query |> Seq.toArray
        with ex ->
            [|{
                CarID = nullable 0
                Car = ""
                }|]
            
    let insertNewCar (data : string) = 
        try
            let schema = dbSchema.GetDataContext()
            let newCar = try
                                JsonConvert.DeserializeObject<carInsertData>(data)
                         with ex ->
                                {
                                    StaffUserName = ""
                                    StaffPassword = ""
                                    Reg = ""
                                    Make = ""
                                    Model = ""
                                    Year = nullable 0s
                                    Colour = ""
                                    BodyType = ""
                                    Doors = nullable 0uy
                                    GearBoxType = ""
                                    Miles = nullable 0s
                                    EngineSize = nullable 0.0
                                    HorsePower = nullable 0s
                                    SixtyTime = nullable 0.0
                                    FuelType = ""
                                    DriveTrainType = ""
                                    Co2 = nullable 0s
                                    Price = nullable 0.0
                                    Dealership = nullable 0uy
                                    Active = nullable false
                                    Image = ""
                                   }

            let creds = 
                            {
                                UserName = newCar.StaffUserName
                                Password = newCar.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match  authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                        
                        schema.CarInsert(newCar.Reg, newCar.Make, newCar.Model, newCar.Year, newCar.Colour, newCar.BodyType, newCar.Doors, newCar.GearBoxType, newCar.Miles, newCar.EngineSize, newCar.HorsePower, newCar.SixtyTime, newCar.FuelType, newCar.DriveTrainType, newCar.Co2, newCar.Price, newCar.Dealership, newCar.Active, newCar.Image) |> ignore
                        HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
        with ex ->
            HttpStatusCode.InternalServerError

    let updateCar (data : string) = 
        try 
            let schema = dbSchema.GetDataContext()
            let carUpdateData = try
                                    JsonConvert.DeserializeObject<fullCarDataUpdate>(data)
                                with ex ->
                                    {
                                        StaffUserName = ""
                                        StaffPassword = ""
                                        CarID = 0
                                        Price = 0.0
                                        Sold = nullable false
                                        Active = nullable false
                                        Dealership = 0
                                       }

            let creds = 
                            {
                                UserName = carUpdateData.StaffUserName
                                Password = carUpdateData.StaffPassword
                            }

            let authcheck = AuthCheck creds

            match  authcheck.Position.Equals("Admin") || authcheck.Position.Equals("Management") || authcheck.Position.Equals("Sales") with
            | true ->
                        schema.CarUpdate(nullable carUpdateData.CarID, nullable carUpdateData.Price, carUpdateData.Sold, carUpdateData.Active, nullable carUpdateData.Dealership) |> ignore
                        HttpStatusCode.OK
            |_-> HttpStatusCode.Forbidden
        with ex ->
            HttpStatusCode.InternalServerError
            