namespace Car

open System
open FSharp.Core

module CCodeTypes =
    
    type staffAuth = {
                        UserName : string
                        Password : string
                        }

    type position = {
                    Position : string
                    }

    type makes = {
                    Make : string
                    }

    type models = {
                    Model : string
                    }
    
    type years = {
                    Year : Nullable<int>
                    }

    type colour = {
                    Colour : string
                    }
    
    type miles = {
                    Miles : Nullable<int>
                    }
    
    type engineSize = {
                    EngineSize : float
                    }
    
    type horsePower = {
                    HorsePower : Nullable<int>
                    }

    type co2 = {
                    Co2 : Nullable<int>
                    }
    
    type price = {
                    Price : Nullable<int>
                    }

    type customerID = {customerID : Nullable<int>}

    type customerCar = {
                        CarID : Nullable<int>
                        Car : string
                        }

    type Dealership = {
                            Dealership : string
                        }

    type Reg = {
                            Reg : string
                        }

    type searchParemeters = {
                                Makes : string[]
                                Years : Nullable<int>[]
                                Colour : string[]
                                Miles : Nullable<int>[]
                                EngineSize : float[]
                                HorsePower : Nullable<int>[]
                                Co2 : Nullable<int>[]
                                Price : Nullable<int>[]
                                }

    type searchParam = {
                        Make : string
                        Model : string
                        YearUpperBound : Nullable<int>
                        YearLowerBound : Nullable<int>
                        Colour : string
                        BodyType : string
                        Doors : Nullable<int>
                        GearBoxType : string
                        MilesUpperBound : Nullable<int>
                        MilesLowerBound : Nullable<int>
                        EngineSizeUpperBound : Nullable<float>
                        EngineSizeLowerBound : Nullable<float>
                        SixtyTimeUpperBound : Nullable<float>
                        SixtyTimeLowerBound : Nullable<float>
                        FuelType : string
                        DriveTrainType : string
                        Co2UpperBound : Nullable<int>
                        Co2LowerBound : Nullable<int>
                        PriceUpperBound : Nullable<float>
                        PriceLowerBound: Nullable<float>
                        }

    type carSearchData = {
                        CarID : Nullable<int>
                        Make : string
                        Model : string
                        Year : Nullable<int16>
                        BodyType : string
                        GearBoxType : string
                        Miles : Nullable<int>
                        EngineSize : Nullable<float>
                        HorsePower : Nullable<int16>
                        FuelType : string
                        Price : Nullable<float>
                        ImageString : string
                    }

    type carSearchAllData = {
                        CarID : Nullable<int>
                        Make : string
                        Model : string
                        Year : Nullable<int16>
                        BodyType : string
                        GearBoxType : string
                        Miles : Nullable<int>
                        EngineSize : Nullable<float>
                        HorsePower : Nullable<int16>
                        FuelType : string
                        Price : Nullable<float>
                        ImageString : string
                        Active : Nullable<bool>
                    }
    
    type carData = {
                        CarID : Nullable<int>
                        Make : string
                        Model : string
                        Year : Nullable<int16>
                        Colour : string
                        BodyType : string
                        Doors: Nullable<byte>
                        GearBoxType : string
                        Miles : Nullable<int>
                        EngineSize : Nullable<float>
                        HorsePower : Nullable<int16>
                        SixtyTime : Nullable<float>
                        FuelType : string
                        DriveTrainType : string
                        Co2 : Nullable<int16>
                        Price : Nullable<float>
                        ImageString : string
                        Name : string
                        Number : string
                        Address : string
                        City : string
                        PostCode : string
                    }
    
    type fullCarData = {
                        CarID : Nullable<int>
                        Car : string
                        Price : Nullable<float>
                        Sold : Nullable<Boolean>
                        Active : Nullable<Boolean>
                        DealershipID : Nullable<byte>
                    }

    type sellCarData = {
                        CarID : Nullable<int>
                        Car : string
                        Price : Nullable<float>
                        Image : string
                    }

    type carID = {
                    carID : int
                    }

    type carInsertData = {
                        StaffUserName : string
                        StaffPassword : string
                        Reg : string
                        Make : string
                        Model : string
                        Year : Nullable<int16>
                        Colour : string
                        BodyType : string
                        Doors: Nullable<byte>
                        GearBoxType : string
                        Miles : Nullable<int16>
                        EngineSize : Nullable<float>
                        HorsePower : Nullable<int16>
                        SixtyTime : Nullable<float>
                        FuelType : string
                        DriveTrainType : string
                        Co2 : Nullable<int16>
                        Price : Nullable<float>
                        Dealership : Nullable<byte>
                        Active : Nullable<bool>
                        Image : string
                    }

    type fullCarDataUpdate = {
                                    StaffUserName : string
                                    StaffPassword : string
                                    CarID : int
                                    Price : float
                                    Sold : Nullable<bool>
                                    Active : Nullable<bool>
                                    Dealership : int
                                    }
