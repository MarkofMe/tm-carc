﻿namespace Car.Controllers

open System
open System.Web.Http
open Car.CCode
open System.Net
open System.Net.Http
open Newtonsoft.Json

type ting = {
        imgData:string
    }

[<RoutePrefixAttribute("CC")>]
type creditCardController() =
    inherit ApiController()

    [<Route("GetSearchParam")>]
    member this.GetSearchParam (insert : HttpRequestMessage ) =
        getSearchParams()

    [<Route("GetModelSearchParam")>]
    member this.GetModelSearchParam (make : string ) =
        getModelSearchParam(make)

    [<Route("Search")>]
    member this.Search (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        Search(jsonContent)

    [<Route("GetSearchAllParam")>]
    member this.GetSearchAllParam (insert : HttpRequestMessage ) =
        getSearchAllParams()

    [<Route("GetModelSearchAllParam")>]
    member this.GetModelSearchAllParam (make : string ) =
        getModelSearchAllParam(make)

    [<Route("ByDealershipID")>]
    member this.ByDealershipID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByDealershipID(jsonContent)

    [<Route("ByReg")>]
    member this.ByReg (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        GetByReg(jsonContent)

    [<Route("SearchAll")>]
    member this.SearchAll (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        SearchAll(jsonContent)

    [<Route("CarDetails")>]
    member this.CarDetails (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        getCarDetails(jsonContent)

    [<Route("FullCarDetails")>]
    member this.FullCarDetails (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        getFullCarDetails(jsonContent)

    [<Route("ByCustomerID")>]
    member this.ByCustomerID (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        ByCustomerID(jsonContent)

    [<Route("InsertNewCar")>]
    member this.InsertNewCar (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        insertNewCar(jsonContent)

    [<Route("UpdateCar")>]
    member this.updateCar (insert : HttpRequestMessage ) =
        let content = insert.Content
        let jsonContent = content.ReadAsStringAsync().Result
        updateCar(jsonContent)
    